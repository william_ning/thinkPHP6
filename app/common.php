<?php
declare (strict_types = 1);
/**
 * 应用公共文件
 */
use think\Exception;
use think\facade\Request;

/**
 * 跨域请求头
 */
function headers()
{
    return [
        'Access-Control-Allow-Headers' => 'Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-CSRF-TOKEN, X-Requested-With, accesslogin',
        'Access-Control-Allow-Origin'  => '*'
    ];
}

/**
 * 失败
 */
function error($message = '失败', $code = 40000, $data = [])
{

    $json = [
        'code'    => $code,
        'message' => $message,
        'data'    => $data
    ];

    return json($json);
}

/**
 * 失败
 */
function errorValidate($message = '验证失败', $code = 50000, $data = [])
{

    $json = [
        'code'    => $code,
        'message' => $message,
        'data'    => $data
    ];

    return json($json);
}

/**
 * 成功
 */
function success($data = [], $message = '成功', $code = 20000)
{

    $json = [
        'code'    => $code,
        'message' => $message,
        'data'    => $data
    ];

    return json($json);
}

/**
 * 抛出异常
 */
function exception($message = '系统繁忙请稍后再试', $code = 10000)
{
    throw new Exception($message, $code);
}

/**
 * 应用前缀
 */
function multiApp($name = 'home')
{
    return env("multi_app.$name");
}

/**
 * 毫秒
 */
function millisecond() {

    list($msec, $sec) = explode(' ', microtime());

    $msectime = (int) sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);

    return $msectime;
}

/**
 *  访问地址
 */
function pathRule()
{
    return Request::controller() . '/' . Request::action();
}

/**
 * 去掉所有空格
 */
function trimAll($str)
{

    $oldchar = [" ","　","\t","\n","\r"];

    $newchar = ["","","","",""];

    return str_replace($oldchar,$newchar,$str);
}

/**
 * 执行一个cURL会话
 */
function curlExec($url = '', $param = [], $http_header = [], $execution_time = 60, $data_type = 0)
{

    ini_set('max_execution_time', '60');
    ini_set('memory_limit', '256M');

    $curl = curl_init();

    $options = [
        CURLOPT_URL            => $url,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        CURLOPT_TIMEOUT        => $execution_time,
        CURLOPT_RETURNTRANSFER => 1
    ];

    if (!empty($http_header)) $options[CURLOPT_HTTPHEADER] = $http_header;

    if (!empty($param)):
        $options[CURLOPT_POST]       = 1;
        $options[CURLOPT_POSTFIELDS] = $param;
    endif;

    curl_setopt_array($curl, $options);

    $curl_exec = curl_exec($curl);

    if($curl_exec != true) exception("调用API时发生错误[" . curl_error($curl) . "]");

    curl_close($curl);

    switch ($data_type) {
        case 0:
            $curl_exec = json_decode($curl_exec, true);
            break;
        case 1:
            libxml_disable_entity_loader(true);
            $curl_exec = json_decode(json_encode(simplexml_load_string($curl_exec, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
            break;
        case 2:
            // 不处理直接返回
            break;
        default:
            exception('API结果处理类型出现异常');
            break;
    }

    return $curl_exec;
}

function explodeMe($string = '', $delimiter = ',')
{
    return $string ? explode($delimiter, $string) : [];
}