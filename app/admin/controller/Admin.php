<?php
declare (strict_types = 1);
/**
 * 管理员
 */
namespace app\admin\controller;

use app\BaseController;
use app\common\facade\admin\AdminAdmin;
use think\exception\ValidateException;
use think\facade\Db;

class Admin extends BaseController
{

    /**
     * 登录
     */
    public function login()
    {

        $param = $this->request->post();

        Db::startTrans();
        try {

            $data['adminInfoMe'] = AdminAdmin::login($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data, '登录成功');
    }

    /**
     * 管理员列表
     */
    public function adminList()
    {

        $param = $this->request->post();

        Db::startTrans();
        try {

            $param['loginInfo'] = $this->request->loginInfo;

            $adminList = AdminAdmin::adminList($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($adminList);
    }

    /**
     * 管理员详情【自己】
     */
    public function adminInfoMe()
    {

        $param = $this->request->post();

        Db::startTrans();
        try {

            $param['id'] = $this->request->loginInfo['id'];

            $data['adminInfoMe'] = AdminAdmin::adminInfo($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data);
    }

    /**
     * 管理员添加【保存】
     */
    public function adminAddSave()
    {

        $param = $this->request->post();

        Db::startTrans();
        try {

            $param['loginInfo'] = $this->request->loginInfo;

            $data['adminAddSave'] = AdminAdmin::adminSave($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data);
    }

    /**
     * 管理员编辑【保存】
     */
    public function adminEditSave()
    {

        $param = $this->request->post();

        Db::startTrans();
        try {

            $param['loginInfo'] = $this->request->loginInfo;

            $data['adminEditSave'] = AdminAdmin::adminSave($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data);
    }

    /**
     * 编辑个人信息【保存】
     */
    public function adminMeEditSave()
    {

        $param = $this->request->post(['password', 'name']);

        Db::startTrans();
        try {

            $param['loginInfo'] = $this->request->loginInfo;

            $param['id'] = $param['loginInfo']['id'];

            $data['adminMeEditSave'] = AdminAdmin::adminSave($param);

            $data['adminInfoMe'] = AdminAdmin::adminInfo($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data);
    }

    /**
     * 退出登录
     */
    public function loginOut()
    {

        $param['accessLogin'] = $this->request->header('accesslogin');

        Db::startTrans();
        try {

            $param['loginInfo'] = $this->request->loginInfo;

            $data['loginOut'] = AdminAdmin::loginOut($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data);
    }

    /**
     * 权限【所有】
     */
    public function ruleAll()
    {

        $param = $this->request->post();

        Db::startTrans();
        try {

            $param['loginInfo'] = $this->request->loginInfo;

            $data['ruleAll'] = AdminAdmin::ruleAll($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data);
    }

    /**
     * 角色【所有】
     */
    public function roleAll()
    {

        $param = $this->request->post();

        Db::startTrans();
        try {

            $param['loginInfo'] = $this->request->loginInfo;

            $data['roleAll'] = AdminAdmin::roleAll($param);

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return errorValidate($e->getError());
        } catch (\Exception $e) {
            Db::rollback();
            return error($e->getMessage(), $e->getCode());
        }

        return success($data);
    }

}
