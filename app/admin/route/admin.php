<?php
/**
 * 管理员
 * User: ZengYou
 * Date: 2020/2/15 22:40
 * Created by : PhpStorm
 */
use think\facade\Route;
use app\common\middleware\Admin;

// 登录
Route::post('login', 'Admin/login')->allowCrossDomain(headers());

Route::group(function () {
    // 管理员列表
    Route::post('adminList', 'adminList');
    // 管理员详情【自己】
    Route::post('adminInfoMe', 'adminInfoMe');
    // 管理员添加【保存】
    Route::post('adminAddSave', 'adminAddSave');
    // 管理员编辑【保存】
    Route::post('adminEditSave', 'adminEditSave');
    // 编辑个人信息【保存】
    Route::post('adminMeEditSave', 'adminMeEditSave');
    // 退出登录
    Route::post('loginOut', 'loginOut');

    // 权限【所有】
    Route::post('ruleAll', 'ruleAll');

    // 角色【所有】
    Route::post('roleAll', 'roleAll');

})->prefix('Admin/')->allowCrossDomain(headers())->middleware(Admin::class);

