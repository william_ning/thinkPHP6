<?php
declare (strict_types = 1);

namespace app\common\facade\model;

use think\Facade;

/**
 * Class AdminModel
 * @see \app\common\model\Admin
 * @package app\common\facade
 * @mixin \app\common\model\Admin
 */
class AdminModel extends Facade
{

    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'app\common\model\Admin';
    }

}
