<?php
declare (strict_types = 1);

namespace app\common\facade\model;

use think\Facade;

/**
 * Class AdminRuleModel
 * @see \app\common\model\AdminRule
 * @package app\common\facade
 * @mixin \app\common\model\AdminRule
 */
class AdminRuleModel extends Facade
{

    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'app\common\model\AdminRule';
    }

}
