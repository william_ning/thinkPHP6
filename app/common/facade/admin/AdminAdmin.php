<?php
declare (strict_types = 1);

namespace app\common\facade\admin;

use think\Facade;

/**
 * Class AdminAdmin
 * @see \app\common\admin\Admin
 * @package facade
 * @mixin \app\common\admin\Admin
 */
class AdminAdmin extends Facade
{

    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'app\common\admin\Admin';
    }

}
