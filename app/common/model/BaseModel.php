<?php
declare (strict_types = 1);
/**
 * 公共
 */
namespace app\common\model;

use think\Model;

class BaseModel extends Model
{

    /**
     * 详情
     */
    public function info($where = [], $field = '*')
    {
        return self::field($field)->where($where)->find();
    }

    /**
     * 详情
     */
    public function infoOrder($where = [], $field = '*', $order = ['id' => 'DESC'])
    {
        $info = self::field($field)->where($where)->order($order)->find();

        if ($info) $info = $info->toArray();

        return $info;
    }

    /**
     * 列表【所有】
     */
    public function listsAll($where = [], $field = [], $order = ['id' => 'DESC'])
    {
        return self::field($field)->where($where)->order($order)->select();
    }

    /**
     * 列表【所有】
     */
    public function listsAllRaw($where = [], $whereRaw = '', $field = [], $order = ['id' => 'DESC'])
    {
        $list = self::field($field)->where($where)->whereRaw($whereRaw)->order($order)->select();

        if ($list) $list = $list->toArray();

        return $list;
    }

    /**
     * 查询限制结果数量列表
     */
    public function listLimit($where = [], $field = true, $offset = 0, $limit = 100, $order = ['id' => 'DESC'])
    {
        $list = self::where($where)->field($field)->limit($offset, $limit)->order($order)->select();

        if ($list) $list = $list->toArray();

        return $list;
    }

    /**
     * 添加保存
     */
    public function addSave($param = [])
    {

        $time              = time();
        $param['addTime']  = $time;
        $param['editTime'] = $time;

        $save = self::save($param);

        if ($save == false) exception('添加失败');

        return $save;
    }

    /**
     * 编辑保存
     */
    public function editSave($param = [], $where = [])
    {

        $param['editTime'] = time();

        $save = self::where($where)->save($param);

        if ($save == false) exception('编辑失败');

        return $save;
    }

    /**
     * 添加保存
     */
    public function addCreate($param = [])
    {

        $time              = time();
        $param['addTime']  = $time;
        $param['editTime'] = $time;

        $save = self::create($param);

        if ($save == false) exception('添加失败');

        return $save;
    }

    /**
     * 编辑
     */
    public function saveUpdate($data = [], $where = [])
    {

        $time              = time();
        $param['editTime'] = $time;

        $save = self::where($where)->update($data);

        if ($save === false) exception('编辑失败');

        return $save;
    }

    /**
     * 自增
     */
    public function incUpdate($where = [], $name = '', $step = 1)
    {

        $time              = time();
        $param['addTime']  = $time;
        $param['editTime'] = $time;

        $save = self::where($where)->inc($name, $step)->update();

        if ($save === false) exception('自增失败');

        return $save;
    }

    /**
     * 如果带主键即批量更新，否则批量新增
     */
    public function savesAll($param = [])
    {
        $saveAll = self::saveAll($param);

        if ($saveAll != true) exception('保存失败');

        return $saveAll;
    }

}
