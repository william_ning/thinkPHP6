<?php
declare (strict_types = 1);
/**
 * 管理员
 */
namespace app\common\model;

class Admin extends BaseModel
{

    public function roleHasOne()
    {
        return $this->hasOne(AdminRole::class, 'id', 'roleId');
    }

    /**
     * 管理员详情
     */
    public function adminInfo($whereAdmin = [])
    {

        return self::with([
            'roleHasOne',
        ])->where($whereAdmin)->find();

    }

    /**
     * 管理员列表
     */
    public function adminList($where = [], int $offsetLimit = 0, int $lengthLimit = 20)
    {

        $list = self::view(
            ['admin' => 'a']
        )->view(
            ['admin_role' => 'ar'],
            [
                'name'   => 'arName',
                'enable' => 'arEnable',
            ],
            'ar.id=a.roleId',
            'LEFT'
        )->where($where)->limit($offsetLimit, $lengthLimit)->order(['a.id' => 'desc'])->select();

        $count = self::view(
            ['admin' => 'a']
        )->view(
            ['admin_role' => 'ar'],
            [
                'ar'     => 'arName',
                'enable' => 'arEnable',
            ],
            'ar.id=a.roleId',
            'LEFT'
        )->where($where)->count('a.id');

        return [
            'list'      => $list,
            'countList' => $count
        ];
    }

}
