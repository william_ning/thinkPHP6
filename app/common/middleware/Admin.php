<?php
declare (strict_types = 1);

namespace app\common\middleware;

use app\common\facade\admin\AdminAdmin;

class Admin
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {

        $param['accessLogin'] = $request->header('accesslogin');

        try {
            $request->loginInfo = AdminAdmin::checkLogin($param);
        } catch (\Exception $e) {
            return error($e->getMessage(), $e->getCode());
        }

        return $next($request);
    }
}
